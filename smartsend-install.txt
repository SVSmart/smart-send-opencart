Smart Send Shipping for OpenCart

Version 0.9.7

== INSTALLATION ==

Simply extract the files in to the OpenCart basic directory.

== SET UP ==

You will need a Smart Send VIP account to use this plugin. Apply for one
(it's easy) at https://www.smartsend.com.au/vipClientEnquiry.cfm

Enable the plugin through the extensions->shipping menu option, then click
'edit' to configure the settings.

Enter your VIP username and password, and the  postcode and town that your
packages will originate from.

== PRODUCT DIMENSIONS ==

The plugin needs to know the size of the packages you are shipping, so you
should set length, width and height for each one. Alternatively, you can
specify 'default' dimensions that will be used if the plugin cannot find
dimensions set for an item in the cart.