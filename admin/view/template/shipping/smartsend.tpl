<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
      	<!-- VIP Username -->
        <tr>
          <td><span class="required">*</span> <?php echo $entry_vipusername ?><br><span class="help">Your Smart Send account username. Visit <a href="https://www.smartsend.com.au/vipClientEnquiry.cfm" target="_blank">Smart Send</a> to register.</span></td>
          <td>
          	<input type="text" name="smartsend_vipusername" id="smartsend_vipusername" value="<?php echo $smartsend_vipusername; ?>"/>
          </td>
        </tr>

      	<!-- VIP Password -->
        <tr>
          <td><span class="required">*</span> <?php echo $entry_vippassword ?></td>
          <td>
          	<input type="text" name="smartsend_vippassword" id="smartsend_vippassword" value="<?php echo $smartsend_vippassword; ?>"/>
          </td>
        </tr>

        <tr>
          <td><span class="required">*</span> <?php echo $entry_postcode; ?><br><span class="help">Where your goods are shipped from.</span></td>
          <td><input type="text" name="smartsend_postcode" size="4" maxlength="4" value="<?php echo $smartsend_postcode; ?>" />
            <br />
            <?php
            if( $error_postcode )
            { ?>
            	<span class="error"><?php echo $error_postcode; ?></span>
            <?php
         	}
         	?></td>
        </tr>

        <tr>
          <td><span class="required">*</span> <?php echo $entry_town; ?></td>
          <td><input type="text" name="smartsend_town" value="<?php echo $smartsend_town; ?>" />
            <br />
            <?php
            if( $error_town )
            { ?>
            	<span class="error"><?php echo $error_town; ?></span>
            <?php
         	}
         	?></td>
        </tr>

        <tr>
          <td><?php echo $entry_package_type; ?></td>
          <td><select name="smartsend_package_type">
              <?php
              foreach( $smartsend_package_types as $ptype )
              	{
              		echo '<option value="'.$ptype.'"';
              		if( $ptype == $smartsend_package_type ) echo ' selected="selected"';
              		echo ">$ptype</option>\n";
              	}
              	?>
            </select></td>
        </tr>

        <tr>
          <td><?php echo $entry_show_results; ?> <span class="help">Smart Send can return multiple shipping options - choose what to show your customers.</span></td>
          <td><select name="smartsend_show_results">
              <?php
              foreach( $smartsend_results_options as $rtype => $rtext )
              	{
              		echo '<option value="'.$rtype.'"';
              		if( $rtype == $smartsend_show_results ) echo ' selected="selected"';
              		echo ">$rtext</option>\n";
              	}
              	?>
            </select></td>
        </tr>

        <tr>
          <td><?php echo $entry_default_dims; ?><br><span class="help">Optional fallback dimensions</span></td>
          <td><input type="text" name="smartsend_default_length" value="<?php echo $smartsend_default_length; ?>" size="3"/> x
          	<input type="text" name="smartsend_default_width" value="<?php echo $smartsend_default_width; ?>" size="3"/> x
          	<input type="text" name="smartsend_default_height" value="<?php echo $smartsend_default_height; ?>" size="3"/></td>
        </tr>


        <tr>
          <td><?php echo $entry_handling_type; ?><br><span class="help">Add a handling fee - flat rate or a percentage of cart total.</span></td>
          <td><select name="smartsend_handling_type">
              <?php
              foreach( $smartsend_handling_types as $htype )
              	{
              		echo '<option value="'.$htype.'"';
              		if( $htype == $smartsend_handling_type ) echo ' selected="selected"';
              		echo ">$htype</option>\n";
              	}
              	?>
            </select></td>
        </tr>
        <tr>
          <td><?php echo $entry_handling_fee; ?></td>
          <td>
          	<input type="text" name="smartsend_handling_fee" id="smartsend_handling_fee" size="4" value="<?php echo $smartsend_handling_fee; ?>" />
          </td>
        </tr>

        <tr>
          <td><?php echo $entry_assurance; ?><br><span class="help">Add transport assurance.</span></td>
          <td><select name="smartsend_assurance">
              <?php
              foreach( $yesOrNo as $yn )
              	{
              		echo '<option value="'.$yn.'"';
              		if( $yn == $smartsend_assurance ) echo ' selected="selected"';
              		echo ">$yn</option>\n";
              	}
              	?>
            </select></td>
        </tr>
        <tr>
          <td><?php echo $entry_assurance_min; ?><br><span class="help">If assurance selected, only add for cart totals over this amount.</span></td>
          <td>
          	<input type="text" name="smartsend_assurance_min" id="smartsend_assurance_min" size="4" value="<?php echo $smartsend_assurance_min; ?>" />
          </td>
        </tr>

        <tr>
          <td><?php echo $entry_receipted; ?><br><span class="help">Offer receipted delivery to customer.</span></td>
          <td><select name="smartsend_receipted">
              <?php
              foreach( $yesOrNo as $yn )
              	{
              		echo '<option value="'.$yn.'"';
              		if( $yn == $smartsend_receipted ) echo ' selected="selected"';
              		echo ">$yn</option>\n";
              	}
              	?>
            </select></td>
        </tr>

        <tr>
          <td><?php echo $entry_tail_pickup; ?><br><span class="help">Request tail-lift pickup for orders with items heavier than this. Leave blank for no tail-lift pickup.</span></td>
          <td>
          	<input type="text" name="smartsend_tail_pickup" id="smartsend_tail_pickup" size="4" value="<?php echo $smartsend_tail_pickup; ?>" /> kg
          </td>
        </tr>
        <tr>
          <td><?php echo $entry_tail_delivery; ?><br><span class="help">Offer receipted delivery to customer if any items over 30kg.</span></td>
          <td><select name="smartsend_tail_delivery">
              <?php
              foreach( $yesOrNo as $yn )
              	{
              		echo '<option value="'.$yn.'"';
              		if( $yn == $smartsend_tail_delivery ) echo ' selected="selected"';
              		echo ">$yn</option>\n";
              	}
              	?>
            </select></td>
        </tr>

          <td><?php echo $entry_status ?></td>
          <td><select name="smartsend_status">
              <?php if ($smartsend_status) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_enabled; ?></option>
              <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="smartsend_sort_order" value="<?php echo $smartsend_sort_order; ?>" size="1" /></td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>
