<?php
// Heading
$_['heading_title'] 			= 'Smart Send';

// Text
$_['text_shipping'] 			= 'Shipping';
$_['text_success']			= 'Successfully Updated Smart Send Options';

// Entry
$_['entry_vipusername']		= 'VIP Username:';
$_['entry_vippassword']		= 'VIP Password:';
$_['entry_postcode']			= 'Origin Post Code:';
$_['entry_town']				= 'Origin Town:';
$_['entry_package_type']	= 'Package Type:';
$_['entry_show_results']	= 'Display Quotes:';
$_['entry_default_dims']	= 'Default Dimensions (L x W x H):';
$_['entry_default_width']	= 'Default Width:';
$_['entry_default_length']	= 'Default Length:';
$_['entry_default_height']	= 'Default Height:';
$_['entry_handling_type']	= 'Add Handling Fee?:';
$_['entry_handling_fee']	= 'Handling Amount:';
$_['entry_assurance']		= 'Transport Assurance:';
$_['entry_assurance_min']	= 'Assurance Minimum:';
$_['entry_receipted']		= 'Receipted Delivery:';
$_['entry_tail_pickup']		= 'Tail-Lift Pickup:';
$_['entry_tail_delivery']	= 'Tail-Lift Delivery:';
$_['entry_status']			= 'Status:';
$_['entry_sort_order']		= 'Sort Order:';

// Errors
$_['error_permission']		= 'Warning: You do not have permission to modify Smart Send shipping!';
$_['error_postcode']			= 'Post Code must be 4 digits!';
$_['error_town']				= 'You must provide the originating town!';
?>