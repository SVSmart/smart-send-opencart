<?php

class ControllerShippingSmartsend extends Controller
{
	private $error = array(); 

	public function index()
	{   
		$this->load->language('shipping/smartsend');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting('auspost', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		// List of config variables; this is simpler for entry_ and smartsend_ data
		$ingest = array(
			'vipusername',
			'vippassword',
			'postcode',
			'town',
			'package_type',
			'show_results',
			'default_dims',
			'default_width',
			'default_length',
			'default_height',
			'handling_type',
			'handling_fee',
			'assurance',
			'assurance_min',
			'receipted',
			'tail_pickup',
			'tail_delivery',
			'status',
			'sort_order'
		);

		$this->data['entry_username'] = $this->language->get('entry_username');
		$this->data['entry_password'] = $this->language->get('entry_password');
		$this->data['entry_postcode'] = $this->language->get('entry_postcode');
		$this->data['entry_town'] = $this->language->get('entry_town');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

		foreach( $ingest as $gest )
		{
			$k = 'entry_' . $gest;
			$this->data[$k] = $this->language->get( $k );
		}


		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if( isset($this->error['warning']) )
			$this->data['error_warning'] = $this->error['warning'];
		else
			$this->data['error_warning'] = '';

		
		if( isset($this->error['postcode']) )
			$this->data['error_postcode'] = $this->error['postcode'];
		else
			$this->data['error_postcode'] = '';
		
		if( isset($this->error['town']) )
			$this->data['error_town'] = $this->error['town'];
		else
			$this->data['error_town'] = '';

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

		$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_shipping'),
      		'separator' => ' :: '
   		);
		
		$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('shipping/smartsend', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('shipping/smartsend', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

		foreach( $ingest as $gest )
		{
			$k = 'smartsend_' . $gest;
			$this->data[$k] = ( isset($this->request->post[$k]) ) ? $this->request->post[$k] : $this->config->get( $k );
		}

		$this->data['smartsend_package_types'] = array(
			'Carton',
			'Satchel/Bag',
			'Tube',
			'Skid',
			'Pallet',
			'Crate',
			'Flat Pack',
			'Roll',
			'Length',
			'Tyre/Wheel',
			'Furniture/Bedding',
			'Envelope'
			);

		$this->data['smartsend_results_options'] = array(
			'all' => 'All',
			'cheapest' => 'Just Cheapest',
			'fastest' => 'Just Fastest'
			);

		$this->data['smartsend_handling_types'] = array(
			'None',
			'Flat Rate',
			'Percentage'
			);

		$this->data['yesOrNo'] = array( 'No', 'Yes' );
 
		$this->template = 'shipping/smartsend.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render());
	}

	private function validate()
	{
		if (!$this->user->hasPermission('modify', 'shipping/smartsend'))
			$this->error['warning'] = $this->language->get('error_permission');

		if( !preg_match('/^\d{4}$/', $this->request->post['smartsend_postcode']) )
			$this->error['postcode'] = $this->language->get('error_postcode');

		if( empty($this->request->post['smartsend_town'] ) )
			$this->error['town'] = $this->language->get('error_town');

		if( !$this->error )
			return true;
		else 
			return false;
	}
}