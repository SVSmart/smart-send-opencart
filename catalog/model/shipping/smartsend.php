<?php

class ModelShippingSmartsend extends Model
{
	protected $_listItems;
	protected $_SmartSendQuote;

	public function getQuote( $address )
	{
		$this->load->language('shipping/smartsend');
		$this->load->model('localisation/country');
		$country_info = $this->model_localisation_country->getCountry($address['country_id']);

		$this->postcodeTo = $address['postcode'];
		$this->suburbTo = $address['city'];
		$this->stateTo = $address['zone_code'];

		$this->_SmartSendQuote = new smartSendUtils( $this->config->get('smartsend_vipusername'), $this->config->get('smartsend_vippassword'));

		$tempUser = $this->config->get('smartsend_vipusername');
		$tempPass = $this->config->get('smartsend_vippassword');
		$temppostcode = $this->config->get('smartsend_postcode');
		$temptown = $this->config->get('smartsend_town');

		// Must be to and from Australia; country_id = 13
		if ($this->config->get('smartsend_status'))
		{
			if( 13 != (int)$address['country_id'] || (int)$country_info['country_id'] != 13 ) return;
			if( 
				!$this->config->get('smartsend_vipusername') || 
				!$this->config->get('smartsend_vippassword') || 
				!$this->config->get('smartsend_postcode') || 
				!$this->config->get('smartsend_town') || 
				!$this->postcodeTo
				)
				return;
		}
		else
			return;

		$packageType = $this->config->get('smartsend_package_type');
		$tailPickup = $this->config->get('smartsend_tail_pickup');
		$tailDelivery = $this->config->get('smartsend_tail_delivery');
		$pickupFlag = $deliveryFlag = false;
		$errors = array();
		// Create the items list
		foreach( $this->cart->getProducts() as $item )
		{
			$itemName = $item['name'];
			$weight 	= ceil($item['weight']);
			if (!$weight) $errors[] = "Cart item '$itemName' has no weight set.";
			$width 	= ( ceil($item['width']) > 0 ) ? ceil($item['width']) : $this->config->get('smartsend_default_width');
			if ( $width < 1 ) $errors[] = "Cart item '$itemName' has no width set.";
			$length 	= ( ceil($item['length']) > 0 ) ? ceil($item['length']) : $this->config->get('smartsend_default_length');
			if ( $length < 1 ) $errors[] = "Cart item '$itemName' has no length set.";
			$height 	= ( ceil($item['height']) > 0 ) ? ceil($item['height']) : $this->config->get('smartsend_default_height');
			if ( $height < 1 ) $errors[] = "Cart item '$itemName' has no height set.";

			// Check for pickup options
			if( $weight >= 30 )
			{
				if( $weight >= $tailPickup ) $pickupFlag = true;
				if( $tailDelivery == 'Yes' ) $deliveryFlag = true;
			}

			foreach( range( 1, $item['quantity'] ) as $blah )
				$this->_listItems[] = array(
					'Description'   => $packageType,
					'Weight'        => $weight,
					'Depth'         => $width,
					'Length'        => $length,
					'Height'        => $height
					);
		}

		if( count($errors) )
			return array(
				'code'       => 'smartsend',
				'title'      => 'Courier',
				'quote'      => array(),
				'sort_order' => $this->config->get('smartsend_sort_order'),
				'error'      => '<h3>Shipping Calculation Errors</h3><ul><li>'.implode( '</li><li>', $errors ) . '</li></ul>'
				);

		if( $pickupFlag || $deliveryFlag )
		{
			if( $pickupFlag ) $this->tailLift = 'PICKUP';

			if( $deliveryFlag )
				if( $pickupFlag ) $this->tailLift = 'BOTH';
					else $this->tailLift = 'DELIVERY';
		}
		else $this->tailLift = 'NONE';

		$cartTotal = $this->cart->getTotal();

		$smartsend_assurance = $this->config->get('smartsend_assurance');
		$smartsend_assurance_min = $this->config->get('smartsend_assurance_min');

		if (!$smartsend_assurance_min)
			$smartsend_assurance_min = 0;

		$this->transportAssurance = 0;
		if( $this->config->get('smartsend_assurance') == 'Yes' )
			if( $cartTotal > $smartsend_assurance_min )
				$this->transportAssurance = $cartTotal;

		$method_data = array();	// The overall rates returned
		$quote_data = array();	// The individual results (can be more than one..)
		$error = false;

		// Fetch the quote!
		$quoteResult = $this->_ssAPI()->ObtainQuoteResult;

		if( $quoteResult->StatusCode != 0 )
		{
			if( $quoteResult->StatusCode == -1 )
				{
					$error = $quoteResult->StatusMessages->string;
					if( is_array( $error ) )
						$error = implode( ', ', $error);
					if( preg_match( '/^Please make sure to select the associated suburb/i', $error ) )
					{
						$suburbs = $this->_SmartSendQuote->getPostcodeSuburbs( $this->postcodeTo );
						$error .= ' Acceptable suburbs are: ' . implode( ', ', $suburbs ) .'.';
					}
				}
			else
				$error = true;

			return $this->_returnError( $error );
		}

		$quote = $quoteResult->Quotes->Quote;

		if( is_object( $quote ) ) $quotes = array( $quote ); // Convert to array if single object
		else $quotes = $quote;

		// How many to display?
		$showResults = $this->config->get('smartsend_show_results');
		if( $showResults == 'cheapest' )
			$showQuotes = array_slice( $quotes, 0, 1 );
		else if( $showResults == 'fastest' )
			$showQuotes = array_slice( $quotes, -1, 1 );
		else $showQuotes = $quotes;

		// Handling?
		$handlingType = $this->config->get('smartsend_handling_type');
		$handlingFee = $this->config->get('smartsend_handling_fee');
		if( $handlingType == 'Flat Rate' )
			$handlingAmt = $handlingFee;
		else if( $handlingType == 'Percentage' )
			$handlingAmt = $cartTotal * $handlingFee / 100;
		else $handlingAmt = 0;

		$ssCode = 0;
		foreach( $showQuotes as $quote )
		{
			$description = $quote->TransitDescription;
			$cost =  $quote->TotalPrice + $handlingAmt;
			$quote_data[] = array(
				'code'         => 'smartsend.'.$ssCode,
				'title'        => $description,
				'cost'         => $cost,
				'tax_class_id' => 0,
				'text'         => $this->currency->format( (float)$cost)
			);
			$ssCode++;
		}

		$method_data = array(
			'code'       => 'smartsend',
			'title'      => 'Courier',
			'quote'      => $quote_data,
			'sort_order' => $this->config->get('smartsend_sort_order'),
			'error'		=> $error
		);
		
		return $method_data;
	}

	protected function _ssAPI()
	{
		global $log;

		$this->_SmartSendQuote->setOptional( 'transportAssurance', $this->__get('transportAssurance') );
		$this->_SmartSendQuote->setOptional( 'tailLift', $this->__get('tailLift') );

		$rxDel = ($this->config->get('smartsend_receipted') == 'Yes' ) ? 1 : 0;
		$this->_SmartSendQuote->setOptional( 'receiptedDelivery', $rxDel );

		$postcodeFrom = $this->config->get('smartsend_postcode');
		$stateFrom = $this->_SmartSendQuote->getState( $postcodeFrom );
		$this->_SmartSendQuote->setFrom( array( $postcodeFrom, $this->config->get('smartsend_town'), $stateFrom) );

		// Brief quote in cart view does not provide town
		$suburbTo = $this->__get('suburbTo');
		$postcodeTo = $this->__get('postcodeTo');
		$stateTo = $this->__get('stateTo');

		if (!$suburbTo )
			$suburbTo = $this->_SmartSendQuote->getFirstTown( $postcodeTo );

		$this->_SmartSendQuote->setTo( array( $postcodeTo, $suburbTo, $stateTo ) );

		foreach( $this->_listItems as $item )
			$this->_SmartSendQuote->addItem( $item );

		$resp = $this->_SmartSendQuote->getQuote();
		return $resp;
	}

	protected function _returnError( $error, $quoteData = '' )
	{
		return array(
			'code'       => 'smartsend',
			'title'      => 'Courier',
			'quote'      => $quoteData,
			'sort_order' => $this->config->get('smartsend_sort_order'),
			'error'      => $error
		);
	}
}


class smartSendUtils
{
	public static $ssPackageTypes = array(
		'Carton',
		'Satchel/Bag',
		'Tube',
		'Skid',
		'Pallet',
		'Crate',
		'Flat Pack',
		'Roll',
		'Length',
		'Tyre/Wheel',
		'Furniture/Bedding',
		'Envelope'
	);
	
	private $liveWSDL = 'http://developer.smartsend.com.au/service.asmx?wsdl';
	private $testWSDL = 'http://uat.smartservice.smartsend.com.au/service.asmx?wsdl ';

	// TTL in days for locations cache
	private $cacheTTL = 1;

	private $locationsCacheFile = DIR_CACHE;

	// Soap connection object
	private $soapClient;

	// Auth details
	private $username = '';
	private $password = '';

	// List of locations
	private $locationList = array();

	// Coming from
	private $postcodeFrom;
	private $suburbFrom;
	private $stateFrom;

	// Going to
	private $postcodeTo;
	private $suburbTo;
	private $stateTo;

	// Arrays of params for quote
	// Each item contains:
	// 	Description: One of -
	// 		Carton, Satchel/Bag, Tube, Skid, Pallet, Crate, Flat Pack, Roll, Length, Tyre/Wheel, Furniture/Bedding, Envelope
	// 	Depth
	// 	Height
	// 	Length
	// 	Weight
	private $quoteItems;

	// Optional promo code
	private $promoCode;

	// Amount to insure
	private $transportAssurance = 0;

	// User type. Options are:
	// EBAY, PROMOTION, VIP
	private $userType = 'VIP';

	// Wether taillift required, if so what and where. Options are:
	// NONE, PICKUP, DELIVERY, BOTH
	private $tailLift = 'NONE';

	// Optional
	private $promotionalCode = '';
	private $onlineSellerId = '';

	private $receiptedDelivery = 'false';

	// Object containing the results of last quote
	private $lastQuoteResults;

	protected $_debug = true;

	// 'Stack' of quotes; first carrier gets the quotes, subsequent ones get it from here
	public static $quoteStack = array();

	/**
	 * Initialise the Smart Send SOAP API
	 *
	 * @param string $username Smart Send VIP username
	 * @param string $password Smart Send VIP password
	 * @param bool   $useTest  Whether to use the test API. Default is false.
	 *
	 * @throws Exception
	 */
	
	public function __construct( $username = NULL, $password = NULL, $useTest = false )
	{
		if( is_null($username) && is_null($password) )
		{
			throw new Exception( 'Missing username and password.');
		}
		$this->username = $username;
		$this->password = $password;

		// Set to test server if username starts with 'test@'
		if( !$useTest && preg_match( '/^test@/', $username ) ) $useTest = true;

		$this->ssWSDL = ( $useTest ) ? $this->testWSDL : $this->liveWSDL;

		$this->soapClient = new SoapClient( $this->ssWSDL );
		$this->locationsCacheFile = dirname(__FILE__) . '/locations.data';
	}

	public function getQuote()
	{
		$required = array(
			'postcodeFrom', 'suburbFrom', 'stateFrom',
			'postcodeTo', 'suburbTo', 'stateTo',
			'userType', 'tailLift'
		);

		foreach( $required as $req )
		{
			if( is_null( $this->$req ) ) throw new Exception( "Cannot get quote without '$req' parameter" );
		}

		if( $this->userType == 'EBAY' && is_null($this->onlineSellerId ))
				throw new Exception( 'Online Seller ID required for Ebay user type.' );

		if( $this->userType == 'PROMOTION' && is_null($this->promotionalCode ))
				throw new Exception( "Promotional code required for user type 'PROMOTION'." );

		if( preg_match( '/false/i', $this->receiptedDelivery ) || !$this->receiptedDelivery )
			$rxDel = 0;
		else
			$rxDel = 1;

		$quoteParams['request'] = array(
			'VIPUsername' => $this->username,
			'VIPPassword' => $this->password,
			'PostcodeFrom' => $this->postcodeFrom,
			'SuburbFrom' => $this->suburbFrom,
			'StateFrom' => $this->stateFrom,
			'PostcodeTo' => $this->postcodeTo,
			'SuburbTo' => $this->suburbTo,
			'StateTo' => $this->stateTo,
			'UserType' => $this->userType,
			'OnlineSellerID' => $this->onlineSellerId,
			'PromotionalCode' => $this->promotionalCode,
			'ReceiptedDelivery' => $rxDel,
			'TailLift' => $this->tailLift,
			'TransportAssurance' => $this->transportAssurance,
			'DeveloperId' => '929fc786-8199-4b81-af60-029e2bca4f39',
			'Items' => $this->quoteItems
		);

		if( $this->_debug ) smart_send_debug_log( 'params', $quoteParams['request'] );

		$this->lastQuoteResults = $this->soapClient->obtainQuote( $quoteParams );

		if( $this->_debug ) smart_send_debug_log( 'results', $this->lastQuoteResults );

		return $this->lastQuoteResults;
	}

	/**
	 * @param array $fromDetails Array of 'from' address details: [ postcode, suburb, state, ]
	 */
	public function setFrom( $fromDetails )
	{
		list( $this->postcodeFrom, $this->suburbFrom, $this->stateFrom ) = $fromDetails;
	}

	/**
	 * @param array $toDetails Array of 'to' address details: [ postcode, suburb, state, ]
	 */
	public function setTo( $toDetails )
	{
		list( $this->postcodeTo, $this->suburbTo, $this->stateTo ) = $toDetails;
	}

	/**
	 * @param $param
	 * @param $value
	 *
	 * @internal param \Set $string optional parameters:
	 *           userType:             EBAY, CORPORATE, PROMOTION, CASUAL, REFERRAL
	 *           onlineSellerID:       Only if userType = EBAY
	 *           promotionalCode:      Only if userType = PROMOTIONAL
	 *           receiptedDelivery:    Customer signs to indicate receipt of package
	 *           tailLift:             For heavy items; either a tail lift truck or extra staff
	 *           transportAssurance:   If insurance is required
	 */
	
	public function setOptional( $param, $value )
	{
		$allowed = array(
			'userType' => array( 'EBAY', 'PROMOTIONAL', 'VIP' ),
			'onlineSellerId' => '',
			'promotionalCode' => '',
			'receiptedDelivery' => array( 'true', 'false', 'TRUE', 'FALSE' ),
			'tailLift' => array( 'NONE', 'PICKUP', 'DELIVERY', 'BOTH' ),
			'transportAssurance' => ''
		);
		if( !in_array( $param, array_keys( $allowed ) ) )
		{
			echo 'Not a settable parameter';
			return;
		} 
		if( is_array( $allowed[$param] ) && !in_array( $value, $allowed[$param]) )
		{
			echo "'$value' is not a valid value for '$param'";
			return;
		}
		$this->$param = $value;
	}

	/**
	 * Add items to be shipped
	 *
	 * @param array $itemData [ Description, Depth, Height, Length, Weight ]
	 *
	 * @throws Exception
	 */
	public function addItem( array $itemData )
	{
		$descriptions = array(
			'Carton',
			'Satchel/Bag',
			'Tube',
			'Skid',
			'Pallet',
			'Crate',
			'Flat Pack',
			'Roll',
			'Length',
			'Tyre/Wheel',
			'Furniture/Bedding',
			'Envelope'
		);
		if( !in_array( $itemData['Description'], $descriptions )) throw new Exception( 'Item must be one of: ' . implode( ', ', $descriptions ) );
		$this->quoteItems[] = $itemData;
	}

	/**
	 * Retrieve official list of locations - postcode, suburb, state
	 *
	 * @param bool $cachedRequested
	 *
	 * @internal param bool $cached true (default) for returning cached data, false for fresh data
	 *
	 * @return array|mixed
	 */
	public function getLocations( $cachedRequested = true )
	{
		if( !$cachedRequested )
			return $this->_locationsToArray();
		else
		{
			if( file_exists( $this->locationsCacheFile ) )
			{
				// Return cached data if not expired
				$fileAge = time() - filemtime( $this->locationsCacheFile );
				if( $fileAge < $this->cacheTTL * 3600 )
					$locationsList = unserialize(file_get_contents($this->locationsCacheFile));
			}
			// Either expired or doesn't exist
			if( !isset( $locationsList ) )
			{
				$locationsList = $this->_locationsToArray();
				file_put_contents( $this->locationsCacheFile, serialize( $locationsList ) );
			}
			return $locationsList;
		}
	}

	// Request locations from SOAP object and convert to an array
	// return: array $this->locationList
	protected function _locationsToArray()
	{
		$locations = $this->soapClient->GetLocations();
		foreach ($locations->GetLocationsResult->Location as $location)
		{
			$postcode = sprintf( "%04d", $location->Postcode );
			$this->locationList[$postcode][] = array( $location->Suburb, $location->State );
		}
		return $this->locationList;
	}

	// Get the first town in the list for a postcode
	public function getFirstTown( $postcode )
	{
		if( !preg_match( '/^\d{4}$/', $postcode ) ) return false;
		$locations = $this->getLocations();
		return trim($locations[$postcode][0][0]);
	}

	// Check if town is at postcode
	public function isTownInPostcode( $postcode, $suburb )
	{
		$locations = $this->getLocations();
		if( isset( $locations[$postcode] ) )
		{
			foreach( $locations[$postcode] as $loc )
			{
				if( strtolower($suburb) == strtolower( $loc[0] ) )
					return true;
			}
			return false;
		}
	}

	// Return array of suburbs at postcode
	public function getPostcodeSuburbs( $postcode )
	{
		$locations = $this->getLocations();
		if( isset( $locations[$postcode] ) )
		{
			$towns = array();
			foreach( $locations[$postcode] as $loc )
				$towns[] = ucwords( strtolower($loc[0]) );
			return array_unique($towns);
		}
	}

	public static function getState( $pcode )
	{
		$first = (int) $pcode[0]; 	// First number
		$pcode = (int) $pcode;		// Type to integer

		if( $first == 1 ) return 'NSW';

		if( $first == 2 ) // ACT or NSW
		{
			if( ( $pcode >= 2600 && $pcode <= 2618 ) || ( $pcode >= 2900 && $pcode <= 2920 ) ) return 'ACT';
			return 'NSW'; // Defaults to..
		}

		if( $pcode < 300 ) return 'ACT';

		if( $first == 3 || $first == 8 ) return 'VIC';

		if( $first == 4 || $first == 9 ) return 'QLD';

		if( $first == 5 ) return 'SA';

		if( $first == 6 ) return 'WA';

		if( $first == 7 ) return 'TAS';

		// ACT's 0200-0299 already caught
		if( $first == 0 ) return 'NT';
	}
}

function smart_send_debug_log( $file, $data )
{
	error_log( date( "Y-m-d H:i:s" ) . " - $_SERVER[REMOTE_ADDR]\n" . print_r( $data, true ), 3, DIR_LOGS . '/log-'.$file.'.log' );
}

function smart_send_slugify( $str )
{
	return strtolower( preg_replace( '/[^a-zA-Z0-9_]+/', '_', $str ) );
}